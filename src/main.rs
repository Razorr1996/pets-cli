#![deny(clippy::all)]
#![warn(clippy::pedantic, clippy::nursery)]

use std::{
    fs, io,
    sync::mpsc,
    thread,
    time::{Duration, Instant},
};

use chrono::{DateTime, Utc};
use crossterm::{
    event::{self, Event as CEvent, KeyCode},
    terminal::{disable_raw_mode, enable_raw_mode},
};
use rand::{distributions::Alphanumeric, Rng};
use serde::{Deserialize, Serialize};
use thiserror::Error;
use tui::{
    backend::CrosstermBackend,
    layout::{Alignment, Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    text::{Line, Span},
    widgets::{
        Block, BorderType, Borders, Cell, List, ListItem, ListState, Paragraph, Row, Table, Tabs,
    },
    Terminal,
};

const DB_PATH: &str = "./data/db.json";

#[derive(Serialize, Deserialize, Clone)]
pub struct Pet {
    id: usize,
    name: String,
    category: String,
    age: usize,
    created_at: DateTime<Utc>,
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("error reading the DB file: {0}")]
    ReadBDError(#[from] io::Error),
    #[error("error reading the DB file: {0}")]
    ParseBDError(#[from] serde_json::Error),
}

enum Event<I> {
    Input(I),
    Tick,
}

#[derive(Copy, Clone, Debug)]
enum MenuItem {
    Home,
    Pets,
}

impl From<MenuItem> for usize {
    fn from(input: MenuItem) -> Self {
        match input {
            MenuItem::Home => 0,
            MenuItem::Pets => 1,
        }
    }
}

// TODO Split function
#[allow(clippy::too_many_lines)]
fn main() -> Result<(), Box<dyn std::error::Error>> {
    enable_raw_mode()?;
    let (tx, rx) = mpsc::channel();

    // Input
    {
        let tick_rate = Duration::from_millis(200);

        thread::spawn(move || {
            let mut last_tick = Instant::now();
            loop {
                let timeout = tick_rate
                    .checked_sub(last_tick.elapsed())
                    .unwrap_or_else(|| Duration::from_secs(0));

                if event::poll(timeout).expect("poll works") {
                    if let CEvent::Key(key) = event::read().expect("can read events") {
                        tx.send(Event::Input(key)).expect("can send events");
                    }
                }

                if last_tick.elapsed() >= tick_rate && tx.send(Event::Tick).is_ok() {
                    last_tick = Instant::now();
                }
            }
        });
    }

    // UI
    {
        let stdout = io::stdout();
        let backend = CrosstermBackend::new(stdout);
        let mut terminal = Terminal::new(backend)?;
        terminal.clear()?;

        let menu_titles = ["Home", "Pets", "Add", "Delete", "Quit"];
        let mut active_menu_item = MenuItem::Home;

        let mut pet_list_state = ListState::default();
        pet_list_state.select(Some(0));

        let copyright = Paragraph::new("Razorr1996 pets-CLI 2022 — all rights reserved")
            .style(Style::default().fg(Color::LightCyan))
            .alignment(Alignment::Center)
            .block(
                Block::default()
                    .borders(Borders::ALL)
                    .title("Copyright")
                    .border_type(BorderType::Plain),
            );

        loop {
            let menu = menu_titles
                .iter()
                .map(|title| {
                    let (first, rest) = title.split_at(1);
                    Line::from(vec![
                        Span::styled(
                            first,
                            Style::default()
                                .fg(Color::Yellow)
                                .add_modifier(Modifier::UNDERLINED),
                        ),
                        Span::raw(rest),
                    ])
                })
                .collect();
            let tabs = Tabs::new(menu)
                .select(active_menu_item.into())
                .block(Block::default().title("Menu").borders(Borders::ALL))
                .highlight_style(Style::default().fg(Color::Yellow))
                .divider(Span::raw("|"));

            let copyright = copyright.clone();

            terminal.draw(|rect| {
                let size = rect.size();
                let chunks = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints(
                        [
                            Constraint::Length(3),
                            Constraint::Min(2),
                            Constraint::Length(3),
                        ]
                        .as_ref(),
                    )
                    .split(size);

                rect.render_widget(tabs, chunks[0]);
                rect.render_widget(copyright, chunks[2]);

                match active_menu_item {
                    MenuItem::Home => rect.render_widget(render_home(), chunks[1]),
                    MenuItem::Pets => {
                        let pets_chunks = Layout::default()
                            .direction(Direction::Horizontal)
                            .constraints(
                                [Constraint::Percentage(20), Constraint::Percentage(80)].as_ref(),
                            )
                            .split(chunks[1]);
                        let (left, right) = render_pets(&pet_list_state);
                        rect.render_stateful_widget(left, pets_chunks[0], &mut pet_list_state);
                        rect.render_widget(right, pets_chunks[1]);
                    },
                }
            })?;

            match rx.recv()? {
                Event::Input(input) => match input.code {
                    KeyCode::Char('q') => {
                        disable_raw_mode()?;
                        terminal.show_cursor()?;

                        return Ok(());
                    },
                    KeyCode::Char('h') => active_menu_item = MenuItem::Home,
                    KeyCode::Char('p') => active_menu_item = MenuItem::Pets,
                    KeyCode::Char('a') => {
                        add_random_pet_to_db().expect("can add new random pet");
                    },
                    KeyCode::Char('d') => {
                        remove_pet_at_index(&mut pet_list_state).expect("can remove pet");
                    },
                    KeyCode::Down => {
                        if let Some(selected) = pet_list_state.selected() {
                            let amount_pets = read_db().expect("can fetch pet list").len();
                            if selected >= amount_pets - 1 {
                                pet_list_state.select(Some(0));
                            } else {
                                pet_list_state.select(Some(selected + 1));
                            }
                        }
                    },
                    KeyCode::Up => {
                        if let Some(selected) = pet_list_state.selected() {
                            let amount_pets = read_db().expect("can fetch pet list").len();
                            if selected > 0 {
                                pet_list_state.select(Some(selected - 1));
                            } else {
                                pet_list_state.select(Some(amount_pets - 1));
                            }
                        }
                    },
                    _ => {},
                },
                Event::Tick => {},
            }
        }
    }
}

fn render_home<'a>() -> Paragraph<'a> {
    let home = Paragraph::new(vec![
        Line::from(vec![Span::raw("")]),
        Line::from(vec![Span::raw("Welcome")]),
        Line::from(vec![Span::raw("")]),
        Line::from(vec![Span::raw("to")]),
        Line::from(vec![Span::raw("")]),
        Line::from(vec![Span::styled("pets-CLI", Style::default().fg(Color::LightBlue))]),
        Line::from(vec![Span::raw("")]),
        Line::from(vec![Span::raw("Press 'p' to access pets, 'a' to add random new pets and 'd' to delete the currently selected pet.")]),
    ])
    .alignment(Alignment::Center)
    .block(
        Block::default()
            .borders(Borders::ALL)
            .title("Home")
            .border_type(BorderType::Plain),
    );
    home
}

fn read_db() -> Result<Vec<Pet>, Error> {
    let db_content = fs::read_to_string(DB_PATH)?;
    let parsed: Vec<Pet> = serde_json::from_str(&db_content)?;
    Ok(parsed)
}

fn render_pets<'a>(pet_list_state: &ListState) -> (List<'a>, Table<'a>) {
    let pets = Block::default()
        .borders(Borders::ALL)
        .title("Pets")
        .border_type(BorderType::Plain);

    let pet_list = read_db().expect("can fetch pet list");

    let items: Vec<_> = pet_list
        .iter()
        .map(|pet| ListItem::new(Line::from(vec![Span::raw(pet.name.clone())])))
        .collect();

    let selected_pet = pet_list
        .get(
            pet_list_state
                .selected()
                .expect("there is always a selected pet"),
        )
        .expect("exists")
        .clone();

    let list = List::new(items).block(pets).highlight_style(
        Style::default()
            .fg(Color::Black)
            .bg(Color::Yellow)
            .add_modifier(Modifier::BOLD),
    );

    let style_bold = Style::default().add_modifier(Modifier::BOLD);

    let pet_detail = Table::new(vec![Row::new(vec![
        Cell::from(Span::raw(selected_pet.id.to_string())),
        Cell::from(Span::raw(selected_pet.name)),
        Cell::from(Span::raw(selected_pet.category)),
        Cell::from(Span::raw(selected_pet.age.to_string())),
        Cell::from(Span::raw(selected_pet.created_at.to_string())),
    ])])
    .header(Row::new(vec![
        Cell::from(Span::styled("ID", style_bold)),
        Cell::from(Span::styled("Name", style_bold)),
        Cell::from(Span::styled("Category", style_bold)),
        Cell::from(Span::styled("Age", style_bold)),
        Cell::from(Span::styled("Created At", style_bold)),
    ]))
    .block(
        Block::default()
            .borders(Borders::ALL)
            .title("Detail")
            .border_type(BorderType::Plain),
    )
    .widths(&[
        Constraint::Percentage(5),
        Constraint::Percentage(20),
        Constraint::Percentage(20),
        Constraint::Percentage(5),
        Constraint::Percentage(20),
    ]);

    (list, pet_detail)
}

fn add_random_pet_to_db() -> Result<Vec<Pet>, Error> {
    let mut rng = rand::thread_rng();
    let mut parsed = read_db()?;

    let category = match rng.gen_range(0..=1) {
        0 => "cats",
        _ => "dogs",
    };

    let random_pet = Pet {
        id: rng.gen_range(0..10_000_000),
        name: rand::thread_rng()
            .sample_iter(Alphanumeric)
            .take(10)
            .map(char::from)
            .collect(),
        category: category.to_owned(),
        age: rng.gen_range(1..15),
        created_at: Utc::now(),
    };

    parsed.push(random_pet);
    fs::write(DB_PATH, serde_json::to_vec_pretty(&parsed)?)?;
    Ok(parsed)
}

fn remove_pet_at_index(pet_list_state: &mut ListState) -> Result<(), Error> {
    if let Some(selected) = pet_list_state.selected() {
        let mut parsed = read_db()?;
        parsed.remove(selected);
        fs::write(DB_PATH, serde_json::to_vec_pretty(&parsed)?)?;
        pet_list_state.select(if parsed.is_empty() {
            None
        } else {
            Some(selected - 1)
        });
    }
    Ok(())
}
